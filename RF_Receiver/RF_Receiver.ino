#include <RCSwitch.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiManager.h>
#include <ArduinoOTA.h>

RCSwitch rcReceiver = RCSwitch();
WiFiClient espClient;
PubSubClient client(espClient);

const char* MQTT_SERVER = "192.168.0.2";
const char* MQTT_USER = "homeassistant";
const char* MQTT_PASSWORD = "2032";
const char* CLIENT_ID = "rf_receiver";
const String SWITCH_TOPIC = "wireless/433/received";

void setup() {
  Serial.begin(115200);
  rcReceiver.enableReceive(D2);
  setup_wifi();
  client.setServer(MQTT_SERVER, 1883);
  setup_OTA();
}

void setup_OTA() {
  ArduinoOTA.setHostname(CLIENT_ID);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
    mqtt_logger("INFO", "Starting ArduinoOTA");
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
    mqtt_logger("INFO", "Ending ArduinoOTA");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  //  mqtt_logger("INFO", "Client ID: " + String(CLIENT_ID) + " || IP Address: " + String(WiFi.localIP()));
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(CLIENT_ID);
}

void publish_message(String topic, String value) {
  Serial.print("Publishing message with value: ");
  Serial.print(value.c_str());
  Serial.print(" to topic: ");
  Serial.println(topic);
  client.publish(topic.c_str(), value.c_str(), true);
  mqtt_logger("INFO", "Publishing message with value: " + value + " to topic: " + topic);
}

void mqtt_logger(String logLevel, String message) {
  //  String logTopic = "log/" + String(CLIENT_ID) + "/" + logLevel;
  //  publish_message(logTopic, message);
}

bool connected = false;
void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(CLIENT_ID, MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      connected = true;
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void scan_rf() {
  Serial.println("Starting RF scan");
  int code = rcReceiver.getReceivedValue();
  if (code != 0) {
    Serial.print("Code received: ");
    Serial.println(code);
    publish_message(SWITCH_TOPIC, String(code).c_str());
  } else {
    Serial.println("Nothing received.");
  }
  rcReceiver.resetAvailable();
}

void loop() {
  ArduinoOTA.handle();
  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();
  scan_rf();
  delay(750);
}
//9955187





